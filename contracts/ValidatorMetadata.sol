pragma solidity ^0.4.18;

import "./SafeMath.sol";
import "./interfaces/IBallotsStorage.sol";
import "./interfaces/IProxyStorage.sol";
import "./interfaces/IKeysManager.sol";
import "./eternal-storage/EternalStorage.sol";

/**
    Валидатор:
        bytes32 firstName - Имя,
        bytes32 lastName - Фамилия,
        string fullAddress - Полный адресс,
        uint256 itn - ИНН,
        uint256 iec - КПП,
        string phoneNumber - Номер телефона,
        uint256 minThreshold

 */

contract ValidatorMetadata is EternalStorage {
    using SafeMath for uint256;

    event MetadataCreated(address indexed miningKey);
    event ChangeRequestInitiated(address indexed miningKey);
    event CancelledRequest(address indexed miningKey);
    event Confirmed(address indexed miningKey, address votingSender);
    event FinalizedChange(address indexed miningKey);
    event RequestForNewProxy(address newProxyAddress);
    event ChangeProxyStorage(address newProxyAddress);

    modifier onlyValidVotingKey(address _votingKey) {
        IKeysManager keysManager = IKeysManager(getKeysManager());
        require(keysManager.isVotingActive(_votingKey));
        _;
    }
    
    modifier onlyFirstTime(address _votingKey) {
        address miningKey = getMiningByVotingKey(_votingKey);
        require(uintStorage[keccak256("validators", miningKey, "createdDate")] == 0);
        _;
    }

    modifier onlyOwner() {
        require(msg.sender == addressStorage[keccak256("owner")]);
        _;
    }

    function proxyStorage() public view returns (address) {
        return addressStorage[keccak256("proxyStorage")];
    }

    function pendingProxyStorage() public view returns (address) {
        return addressStorage[keccak256("pendingProxyStorage")];
    }

    function validators(address _miningKey) public view returns (
        bytes32 firstName,
        bytes32 lastName,
        string fullAddress,
        uint256 itn,
        uint256 iec,
        string phoneNumber,
        uint256 minThreshold
    ) {
        firstName = bytes32Storage[keccak256("validators", _miningKey, "firstName")];
        lastName = bytes32Storage[keccak256("validators", _miningKey, "lastName")];
        fullAddress = stringStorage[keccak256("validators", _miningKey, "fullAddress")];
        itn = uintStorage[keccak256("validators", _miningKey, "itn")];
        iec = uintStorage[keccak256("validators", _miningKey, "iec")];
        phoneNumber = stringStorage[keccak256("validators", _miningKey, "phoneNumber")];
        minThreshold = uintStorage[keccak256("validators", _miningKey, "minThreshold")];
    }

    function pendingChanges(address _miningKey) public view returns (
        bytes32 firstName,
        bytes32 lastName,
        string fullAddress,
        uint256 itn,
        uint256 iec,
        string phoneNumber,
        uint256 minThreshold
    ) {
        firstName = bytes32Storage[keccak256("pendingChanges", _miningKey, "firstName")];
        lastName = bytes32Storage[keccak256("pendingChanges", _miningKey, "lastName")];
        fullAddress = stringStorage[keccak256("pendingChanges", _miningKey, "fullAddress")];
        itn = uintStorage[keccak256("pendingChanges", _miningKey, "itn")];
        iec = uintStorage[keccak256("pendingChanges", _miningKey, "iec")];
        phoneNumber = stringStorage[keccak256("pendingChanges", _miningKey, "phoneNumber")];
        minThreshold = uintStorage[keccak256("pendingChanges", _miningKey, "minThreshold")];
    }

    function confirmations(address _miningKey) public view returns (
        uint256 count,
        address[] voters
    ) {
        return (
            uintStorage[keccak256("confirmations", _miningKey, "count")],
            addressArrayStorage[keccak256("confirmations", _miningKey, "voters")]
        );
    }

    function pendingProxyConfirmations(address _newProxyAddress) public view returns (
        uint256 count,
        address[] voters
    ) {
        bytes32 countHash = keccak256("pendingProxyConfirmations", _newProxyAddress, "count");
        bytes32 votersHash = keccak256("pendingProxyConfirmations", _newProxyAddress, "voters");

        return (
            uintStorage[countHash],
            addressArrayStorage[votersHash]
        );
    }

    function setProxyAddress(address _newProxyAddress) public onlyValidVotingKey(msg.sender) {
        bytes32 pendingProxyStorageHash =
            keccak256("pendingProxyStorage");

        require(addressStorage[pendingProxyStorageHash] == address(0));
        
        addressStorage[pendingProxyStorageHash] = _newProxyAddress;
        uintStorage[keccak256("pendingProxyConfirmations", _newProxyAddress, "count")] = 1;
        addressArrayStorage[keccak256("pendingProxyConfirmations", _newProxyAddress, "voters")].push(msg.sender);
        
        RequestForNewProxy(_newProxyAddress);
    }

    function confirmNewProxyAddress(address _newProxyAddress)
        public
        onlyValidVotingKey(msg.sender)
    {
        bytes32 proxyStorageHash = keccak256("proxyStorage");
        bytes32 pendingProxyStorageHash = keccak256("pendingProxyStorage");
        bytes32 countHash = keccak256("pendingProxyConfirmations", _newProxyAddress, "count");
        bytes32 votersHash = keccak256("pendingProxyConfirmations", _newProxyAddress, "voters");
        
        require(addressStorage[pendingProxyStorageHash] != address(0));
        require(!isAddressAlreadyVotedProxy(_newProxyAddress, msg.sender));

        uintStorage[countHash] = uintStorage[countHash].add(1);
        addressArrayStorage[votersHash].push(msg.sender);
        
        if (uintStorage[countHash] >= 3) {
            addressStorage[proxyStorageHash] = _newProxyAddress;
            addressStorage[pendingProxyStorageHash] = address(0);
            delete uintStorage[countHash];
            delete addressArrayStorage[votersHash];
            ChangeProxyStorage(_newProxyAddress);
        }
        
        Confirmed(_newProxyAddress, msg.sender);
    }

    function initMetadata(
        bytes32 _firstName,
        bytes32 _lastName,
        string _fullAddress,
        uint256 _minThreshold,
        string _phoneNumber,
        address _miningKey
    )
        public
        onlyOwner
    {
        IKeysManager keysManager = IKeysManager(getKeysManager());
        require(keysManager.isMiningActive(_miningKey));
        require(uintStorage[keccak256("validators", _miningKey)] == 0);
        require(!boolStorage[keccak256("initMetadataDisabled")]);
        bytes32Storage[keccak256("validators", _miningKey, "firstName")] = _firstName;
        bytes32Storage[keccak256("validators", _miningKey, "lastName")] = _lastName;
        stringStorage[keccak256("validators", _miningKey, "fullAddress")] = _fullAddress;
        uintStorage[keccak256("validators", _miningKey, "minThreshold")] = _minThreshold;
        stringStorage[keccak256("validators", _miningKey, "phoneNumber")] = _phoneNumber;
    }

    function initMetadataDisable() public onlyOwner {
        boolStorage[keccak256("initMetadataDisabled")] = true;
    }

    function createMetadata(
        bytes32 _firstName,
        bytes32 _lastName,
        string _fullAddress,
        uint256 _itn,
        uint256 _iec,
        string _phoneNumber
    )
        public
        onlyValidVotingKey(msg.sender)
        onlyFirstTime(msg.sender)
    {
        address miningKey = getMiningByVotingKey(msg.sender);
        bytes32Storage[keccak256("validators", miningKey, "firstName")] = _firstName;
        bytes32Storage[keccak256("validators", miningKey, "lastName")] = _lastName;
        stringStorage[keccak256("validators", miningKey, "fullAddress")] = _fullAddress;

        uintStorage[keccak256("validators", miningKey, "itn")] = _itn;
        uintStorage[keccak256("validators", miningKey, "iec")] = _iec;
        stringStorage[keccak256("validators", miningKey, "phoneNumber")] = _phoneNumber;
        
        uintStorage[keccak256("validators", miningKey, "createdDate")] = getTime();
        
        uintStorage[keccak256("validators", miningKey, "minThreshold")] = getMinThreshold();
        MetadataCreated(miningKey);
    }

    function changeRequest(
        bytes32 _firstName,
        bytes32 _lastName,
        string _fullAddress,
        uint256 _itn,
        uint256 _iec,
        string _phoneNumber
    )
        public
        onlyValidVotingKey(msg.sender)
        returns(bool)
    {
        address miningKey = getMiningByVotingKey(msg.sender);
        return changeRequestForValidator(
            _firstName,
            _lastName,
            _fullAddress,
            _itn,
            _iec,
            _phoneNumber,
            miningKey
        );
    }

    function changeRequestForValidator(
        bytes32 _firstName,
        bytes32 _lastName,
        string _fullAddress,
        uint256 _itn,
        uint256 _iec,
        string _phoneNumber,
        address _miningKey
    )
        public
        onlyValidVotingKey(msg.sender)
        returns(bool) 
    {
        bytes32Storage[keccak256("pendingChanges", _miningKey, "firstName")] = _firstName;
        bytes32Storage[keccak256("pendingChanges", _miningKey, "lastName")] = _lastName;
        stringStorage[keccak256("pendingChanges", _miningKey, "fullAddress")] = _fullAddress;

        uintStorage[keccak256("pendingChanges", _miningKey, "itn")] = _itn;
        uintStorage[keccak256("pendingChanges", _miningKey, "iec")] = _iec;
        stringStorage[keccak256("pendingChanges", _miningKey, "phoneNumber")] = _phoneNumber;

        uintStorage[keccak256("pendingChanges", _miningKey, "minThreshold")] = uintStorage[keccak256("validators", _miningKey, "minThreshold")];
        
        delete uintStorage[keccak256("confirmations", _miningKey, "count")];
        delete addressArrayStorage[keccak256("confirmations", _miningKey, "voters")];
        
        ChangeRequestInitiated(_miningKey);
        return true;
    }

    function cancelPendingChange() public onlyValidVotingKey(msg.sender) returns(bool) {
        address miningKey = getMiningByVotingKey(msg.sender);
        _deletePendingChange(miningKey);
        CancelledRequest(miningKey);
        return true;
    }

    function isAddressAlreadyVoted(address _miningKey, address _voter) public view returns(bool) {
        bytes32 hash = keccak256("confirmations", _miningKey, "voters");
        uint256 length = addressArrayStorage[hash].length;
        for (uint256 i = 0; i < length; i++) {
            if (addressArrayStorage[hash][i] == _voter) {
                return true;   
            }
        }
        return false;
    }

    function isAddressAlreadyVotedProxy(address _newProxy, address _voter) public view returns(bool) {
        bytes32 hash = keccak256("pendingProxyConfirmations", _newProxy, "voters");
        uint256 length = addressArrayStorage[hash].length;
        for (uint256 i = 0; i < length; i++) {
            if (addressArrayStorage[hash][i] == _voter) {
                return true;   
            }
        }
        return false;
    }

    function confirmPendingChange(address _miningKey) public onlyValidVotingKey(msg.sender) {
        bytes32 votersHash = keccak256("confirmations", _miningKey, "voters");
        bytes32 countHash = keccak256("confirmations", _miningKey, "count");
        
        require(!isAddressAlreadyVoted(_miningKey, msg.sender));
        require(addressArrayStorage[votersHash].length <= 50); // no need for more confirmations

        address miningKey = getMiningByVotingKey(msg.sender);
        require(miningKey != _miningKey);

        addressArrayStorage[votersHash].push(msg.sender);
        uintStorage[countHash] = uintStorage[countHash].add(1);
        Confirmed(_miningKey, msg.sender);
    }

    function finalize(address _miningKey) public onlyValidVotingKey(msg.sender) {
        uint256 count = uintStorage[keccak256("confirmations", _miningKey, "count")];
        uint256 minThreshold = uintStorage[keccak256("pendingChanges", _miningKey, "minThreshold")];

        require(count >= minThreshold);

        bytes32Storage[keccak256("validators", _miningKey, "firstName")] = bytes32Storage[keccak256("pendingChanges", _miningKey, "firstName")];
        bytes32Storage[keccak256("validators", _miningKey, "lastName")] = bytes32Storage[keccak256("pendingChanges", _miningKey, "lastName")];
        stringStorage[keccak256("validators", _miningKey, "fullAddress")] = stringStorage[keccak256("pendingChanges", _miningKey, "fullAddress")];
        
        uintStorage[keccak256("validators", _miningKey, "itn")] = uintStorage[keccak256("pendingChanges", _miningKey, "itn")];
        uintStorage[keccak256("validators", _miningKey, "iec")] = uintStorage[keccak256("pendingChanges", _miningKey, "iec")];
        stringStorage[keccak256("validators", _miningKey, "phoneNumber")] = stringStorage[keccak256("pendingChanges", _miningKey, "phoneNumber")];
        uintStorage[keccak256("validators", _miningKey, "minThreshold")] = uintStorage[keccak256("pendingChanges", _miningKey, "minThreshold")];
        
        _deletePendingChange(_miningKey);
        FinalizedChange(_miningKey);
    }

    function getMiningByVotingKey(address _votingKey) public view returns(address) {
        IKeysManager keysManager = IKeysManager(getKeysManager());
        return keysManager.getMiningKeyByVoting(_votingKey);
    }

    function getTime() public view returns(uint256) {
        return now;
    }

    function getMinThreshold() public view returns(uint256) {
        uint8 thresholdType = 2;
        IBallotsStorage ballotsStorage = IBallotsStorage(getBallotsStorage());
        return ballotsStorage.getBallotThreshold(thresholdType);
    }

    function getBallotsStorage() public view returns(address) {
        return IProxyStorage(proxyStorage()).getBallotsStorage();
    }

    function getKeysManager() public view returns(address) {
        return IProxyStorage(proxyStorage()).getKeysManager();
    }

    function _deletePendingChange(address _miningKey) private {
        delete bytes32Storage[keccak256("pendingChanges", _miningKey, "firstName")];
        delete bytes32Storage[keccak256("pendingChanges", _miningKey, "lastName")];
        delete stringStorage[keccak256("pendingChanges", _miningKey, "fullAddress")];
        delete uintStorage[keccak256("pendingChanges", _miningKey, "itn")];
        delete uintStorage[keccak256("pendingChanges", _miningKey, "iec")];
        delete stringStorage[keccak256("pendingChanges", _miningKey, "phoneNumber")];
        delete uintStorage[keccak256("pendingChanges", _miningKey, "minThreshold")];
    }

}